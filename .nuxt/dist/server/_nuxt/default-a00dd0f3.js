import { _ as __nuxt_component_0$1 } from "./nuxt-img-926a881a.js";
import { mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderSlot } from "vue/server-renderer";
import { _ as _export_sfc } from "../server.mjs";
import "flowbite";
import "./index-6a088328.js";
import "@unhead/shared";
import "destr";
import "devalue";
import "defu";
import "klona";
import "ufo";
import "hookable";
import "h3";
import "ofetch";
import "#internal/nitro";
import "unctx";
import "unhead";
import "vue-router";
const _sfc_main$1 = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  const _component_NuxtImg = __nuxt_component_0$1;
  _push(`<footer${ssrRenderAttrs(mergeProps({ class: "p-4 bg-mainBG contrast-100 md:p-8 lg:p-10" }, _attrs))}><div class="mx-auto max-w-screen-xl text-center"><a href="/" class="flex justify-center items-center text-2xl font-semibold text-neutral-100 dark:text-neutral-100">`);
  _push(ssrRenderComponent(_component_NuxtImg, {
    src: "/logoOne.svg",
    alt: "saveefforts-logo",
    class: "mr-2 w-8 h-8 aspect-4/3"
  }, null, _parent));
  _push(` SaveEfforts.com </a><p class="my-6 text-neutral-100 dark:text-gray-400">SaveEfforts is brand name of Qualyval Ltd is a company registered in England &amp; Wales. We can be contacted at aiautomation@saveefforts.com or +441908103333</p><span class="text-sm text-neutral-200 sm:text-center dark:text-gray-400">© 2023-2024 <a href="/privacy-policy" class="hover:underline">SaveEfforts™</a>. All Rights Reserved.</span></div></footer>`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/FooterComponent.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_0 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender]]);
const default_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  __name: "default",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      const _component_FooterComponent = __nuxt_component_0;
      _push(`<div${ssrRenderAttrs(_attrs)}>`);
      ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
      _push(ssrRenderComponent(_component_FooterComponent, null, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("layouts/default.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
//# sourceMappingURL=default-a00dd0f3.js.map
