export default {
  "_index.86ad9007.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.86ad9007.js",
    "imports": [
      "_vue.f36acd1f.1c99013c.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_nuxt-link.8127bd89.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "nuxt-link.8127bd89.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_vue.f36acd1f.1c99013c.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "vue.f36acd1f.1c99013c.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "layouts/default.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "default.1b49ef6c.css",
    "src": "layouts/default.css"
  },
  "layouts/default.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "default.5033e467.js",
    "imports": [
      "_index.86ad9007.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_vue.f36acd1f.1c99013c.js"
    ],
    "isDynamicEntry": true,
    "src": "layouts/default.vue"
  },
  "default.1b49ef6c.css": {
    "file": "default.1b49ef6c.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "error-404.95c28eb4.css",
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-404.css"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "error-404.7029dbc4.js",
    "imports": [
      "_nuxt-link.8127bd89.js",
      "_vue.f36acd1f.1c99013c.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue"
  },
  "error-404.95c28eb4.css": {
    "file": "error-404.95c28eb4.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "error-500.e798523c.css",
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-500.css"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "error-500.12bd8029.js",
    "imports": [
      "_vue.f36acd1f.1c99013c.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue"
  },
  "error-500.e798523c.css": {
    "file": "error-500.e798523c.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/nuxt/dist/app/entry.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "dynamicImports": [
      "layouts/default.vue",
      "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue",
      "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue"
    ],
    "file": "entry.72af957e.js",
    "isEntry": true,
    "src": "node_modules/nuxt/dist/app/entry.js",
    "_globalCSS": true
  },
  "pages/index.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "index.1c403d04.css",
    "src": "pages/index.css"
  },
  "pages/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "index.4d6fb7be.js",
    "imports": [
      "_index.86ad9007.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_vue.f36acd1f.1c99013c.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/index.vue"
  },
  "index.1c403d04.css": {
    "file": "index.1c403d04.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "pages/privacy-policy.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "privacy-policy.5956fc3e.js",
    "imports": [
      "_nuxt-link.8127bd89.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/privacy-policy.vue"
  }
}