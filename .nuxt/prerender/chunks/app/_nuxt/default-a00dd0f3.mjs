import { _ as __nuxt_component_0$1 } from './nuxt-img-926a881a.mjs';
import { useSSRContext, mergeProps } from 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderSlot, ssrRenderComponent } from 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/vue/server-renderer/index.mjs';
import { _ as _export_sfc } from '../server.mjs';
import './index-6a088328.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/@unhead/shared/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/defu/dist/defu.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/ufo/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/h3/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/ofetch/dist/node.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/hookable/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unctx/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unhead/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/vue-router/dist/vue-router.node.mjs';
import '../../nitro/nitro-prerenderer.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/destr/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/scule/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/klona/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/ohash/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/drivers/fs.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/drivers/memory.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/drivers/lru-cache.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/drivers/fs-lite.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/radix3/dist/index.mjs';
import 'node:fs';
import 'node:url';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/pathe/dist/index.mjs';

const _sfc_main$1 = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  const _component_NuxtImg = __nuxt_component_0$1;
  _push(`<footer${ssrRenderAttrs(mergeProps({ class: "p-4 bg-mainBG contrast-100 md:p-8 lg:p-10" }, _attrs))}><div class="mx-auto max-w-screen-xl text-center"><a href="/" class="flex justify-center items-center text-2xl font-semibold text-neutral-100 dark:text-neutral-100">`);
  _push(ssrRenderComponent(_component_NuxtImg, {
    src: "/logoOne.svg",
    alt: "saveefforts-logo",
    class: "mr-2 w-8 h-8 aspect-4/3"
  }, null, _parent));
  _push(` SaveEfforts.com </a><p class="my-6 text-neutral-100 dark:text-gray-400">SaveEfforts is brand name of Qualyval Ltd is a company registered in England &amp; Wales. We can be contacted at aiautomation@saveefforts.com or +441908103333</p><span class="text-sm text-neutral-200 sm:text-center dark:text-gray-400">\xA9 2023-2024 <a href="/privacy-policy" class="hover:underline">SaveEfforts\u2122</a>. All Rights Reserved.</span></div></footer>`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/FooterComponent.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_0 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender]]);
const _sfc_main = {
  __name: "default",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      const _component_FooterComponent = __nuxt_component_0;
      _push(`<div${ssrRenderAttrs(_attrs)}>`);
      ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
      _push(ssrRenderComponent(_component_FooterComponent, null, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("layouts/default.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=default-a00dd0f3.mjs.map
