import { defineEventHandler, readBody, setResponseStatus } from 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/h3/dist/index.mjs';
import * as Realm from 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/realm-web/dist/bundle.cjs.js';
import { u as useRuntimeConfig } from './nitro/nitro-prerenderer.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/node-fetch-native/dist/polyfill.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/ofetch/dist/node.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/destr/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unenv/runtime/fetch/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/hookable/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/scule/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/klona/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/defu/dist/defu.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/ohash/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/ufo/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/dist/index.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/drivers/fs.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/drivers/memory.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/drivers/lru-cache.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/unstorage/drivers/fs-lite.mjs';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/radix3/dist/index.mjs';
import 'node:fs';
import 'node:url';
import 'file://E:/Qualyval/Real-Project/planning-permission-page-3/node_modules/pathe/dist/index.mjs';

function useRealm() {
  const app = new Realm.App({
    id: useRuntimeConfig().public.appId
  });
  let mongo = null;
  if (app == null ? void 0 : app.currentUser) {
    mongo = app.currentUser.mongoClient("mongodb-atlas");
  }
  return {
    app,
    Realm,
    mongo
  };
}
async function getServerUser() {
  const { app, Realm: Realm2 } = useRealm();
  const credentials = Realm2.Credentials.apiKey(useRuntimeConfig().public.serverApiKey);
  const user = await app.logIn(credentials);
  return user;
}

const saveMessage_post = defineEventHandler(async (event) => {
  var _a, _b;
  const { app } = useRealm();
  let user = app == null ? void 0 : app.currentUser;
  if (!user) {
    user = await getServerUser();
  }
  const mongo = user == null ? void 0 : user.mongoClient("mongodb-atlas");
  const database = useRuntimeConfig().public.db;
  const col = (_a = useRuntimeConfig().public.collection) != null ? _a : "users";
  const userCollection = (_b = mongo == null ? void 0 : mongo.db(database)) == null ? void 0 : _b.collection(col);
  try {
    const { name, email, message } = await readBody(event);
    await userCollection.insertOne({ name, email, message });
    return {
      message: "Message has been sent"
    };
  } catch (error) {
    console.log(error);
    setResponseStatus(event, 500);
    return {
      error: "Server Error"
    };
  }
});

export { saveMessage_post as default };
//# sourceMappingURL=saveMessage.post.mjs.map
