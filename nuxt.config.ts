// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ["@nuxtjs/tailwindcss", "@nuxt/image"],
  runtimeConfig: {
    public: {
      serverApiKey: process.env.NUXT_PUBLIC_SERVER_API_KEY,
      appId: process.env.NUXT_PUBLIC_APP_ID,
      db: process.env.NUXT_PUBLIC_DB,
      collection: process.env.NUXT_PUBLIC_COLLECTION,
      googleAnalyticsId: "",
    },
  },
  app: {
    head: {
      title: "SaveEfforts: AI & Automation",
      meta: [
        {
          name: "description",
          content:
            "We assist businesses in achieving growth, solving problems, enhancing productivity, and optimising efficiency through the application of AI, Automation, and Digital technologies.",
        },
      ],
      charset: "utf-8",
      viewport: "width=device-width, initial-scale=1",
      htmlAttrs: {
        lang: "en",
      },
      link: [
        { rel: "icon", type: "image/svg+xml", href: "/logo-saveefforts-2.png" },
      ],
    },
  },
  image: {
    provider: "netlify",
  },
  plugins: [],
});
