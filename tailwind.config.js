/** @type {import('tailwindcss').Config} */
export default {
  content: [

    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./node_modules/flowbite/**/*.{js,ts}"

  ],
  theme: {
    extend: {
      colors: {
        primary: { "50": "#eff6ff", "100": "#dbeafe", "200": "#bfdbfe", "300": "#93c5fd", "400": "#60a5fa", "500": "#3b82f6", "600": "#2563eb", "700": "#1d4ed8", "800": "#1e40af", "900": "#1e3a8a", "950": "#172554" },
        secondary: {"50":"#f9fafb","100":"#f3f4f6","200":"#e5e7eb","300":"#d1d5db","400":"#9ca3af","500":"#6b7280","600":"#4b5563","700":"#374151","800":"#1f2937","900":"#111827","950":"#030712"},
        lightYellow: '#FFC03D',
        darkYellow: '#f6b245',
        mainBG : '#000837',
        mainBG2 : '#050A30',
        lightBG : '#253052',
        AiAuto : '#A358DF',
        platfrom : '#00CC6F',
        customise : '#2BB9FF',
        figmaCol : '#4C1D95',
        frontEnd : '#1A5E52',
        backEnd : '#1E429F',
        articleCol : '#e4c4ea',
      },
      aspectRatio: {
        '4/3': '4 / 3',
      },
    }

  },
  plugins: [
    require('flowbite/plugin')

  ],
}

